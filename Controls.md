#Gampad 1
=============
* **Left wheel** - Left Joystick
* **Right wheel** - Right Joystick
* **dpad up** - Change to 100% power
* **dpad down** - Change to 25% power
* **dpad left** - Change to 75% power
* **dpad right** - Change to 50% power


#Gamepad 2
===============
* **right trigger** - arm up 
* **left trigger** - arm down
* **right bumper** -  close claw
* **left bumper** - open claw
* **button A** - pulley loosen
* **button B** -  pulley tighten
