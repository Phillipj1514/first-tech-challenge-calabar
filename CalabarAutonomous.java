/* Copyright (c) 2017 FIRST. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted (subject to the limitations in the disclaimer below) provided that
 * the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of FIRST nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
 * LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DigitalChannel;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;


/**
 * This file contains an minimal example of a Linear "OpMode". An OpMode is a 'program' that runs in either
 * the autonomous or the teleop period of an FTC match. The names of OpModes appear on the menu
 * of the FTC Driver Station. When an selection is made from the menu, the corresponding OpMode
 * class is instantiated on the Robot Controller and executed.
 *
 * This particular OpMode just executes a basic Tank Drive Teleop for a two wheeled robot
 * It includes all the skeletal structure that all linear OpModes contain.
 *
 * Use Android Studios to Copy this Class, and Paste it into your team's code folder with a new name.
 * Remove or comment out the @Disabled line to add this opmode to the Driver Station OpMode list
 */

@TeleOp(name="CBAR_driver_control", group="Linear Opmode")
public class CalabarDriverControl extends LinearOpMode {

    // Declare motors and sesors
    private ElapsedTime runtime = new ElapsedTime();
    private DcMotor leftDrive = null;
    private DcMotor rightDrive = null;
    private DcMotor arm = null;
    public DcMotor pull = null;
    private Servo claw1= null;
    private Servo claw2= null;
    ColorSensor sensorColor;
    DistanceSensor sensorDistance;
    DigitalChannel digitalTouch;

    // declare other useful variables
    public static final double MAX_POSITION= 0.9;
    public static final double Arm_rest= 0.4;
    public static final double MIN_POSITION=0;

    //    boolean increase_power = gamepad1.dpad_up;
//    boolean decrease_power = gamepad1.dpad_down;
//    boolean half_power = gamepad1.dpad_right;
//    boolean power = gamepad1.dpad_left;
    int speed_max;


    @Override
    public void runOpMode() {
        telemetry.addData("Status", "Initialized");
        telemetry.update();

        // Initialize the hardware variables. Note that the strings used here as parameters
        // to 'get' must correspond to the names assigned during the robot configuration
        // step (using the FTC Robot Controller app on the phone).
        leftDrive  = hardwareMap.get(DcMotor.class, "D1");
        rightDrive = hardwareMap.get(DcMotor.class, "D2");
        arm = hardwareMap.get(DcMotor.class, "arm");
        claw1 = hardwareMap.get(Servo.class, "claw01");
        claw2 = hardwareMap.get(Servo.class, "claw02");
        pull = hardwareMap.get(DcMotor.class, "pull");
        sensorColor = hardwareMap.get(ColorSensor.class, "sensor_color_distance");
        sensorDistance = hardwareMap.get(DistanceSensor.class, "sensor_color_distance");
        digitalTouch = hardwareMap.get(DigitalChannel.class, "sensor_digital");

        // set the digital channel to input.
        digitalTouch.setMode(DigitalChannel.Mode.INPUT);

        speed_max=2;

        // Most robots need the motor on one side to be reversed to drive forward
        // Reverse the motor that runs backwards when connected directly to the battery
        leftDrive.setDirection(DcMotor.Direction.REVERSE);
        rightDrive.setDirection(DcMotor.Direction.FORWARD);

        // initialize arm in default position
        claw1.setPosition(Arm_rest);
        claw2.setPosition(Arm_rest);

        // Wait for the game to start (driver presses PLAY)
        waitForStart();
        runtime.reset();

        // run until the end of the match (driver presses STOP)
        while (opModeIsActive()) {
            driving();
            arm_lift();
            claw();
            pull_system();
            telemetry.addData("Status", "Run Time: " + runtime.toString());
            telemetry.update();
        }
    }

    // code for the driving
    public void driving(){
        double leftPower = 0;
        double rightPower =0;

        boolean run_power = gamepad1.dpad_up;
        boolean run_power2 = gamepad1.dpad_down;
        boolean run_power3 = gamepad1.dpad_left;
        boolean run_power4 = gamepad1.dpad_right;

        if(run_power){
            speed_max=2;
        }else if(run_power2){
            speed_max=1;
        }else if(run_power3)
        {speed_max =4;
        }else if(run_power4){
            speed_max=3;
        }

        switch (speed_max){
            case 1:
                leftPower = gamepad1.left_stick_y*0.25;
                rightPower = gamepad1.right_stick_y*0.25;
                telemetry.addData("drive_speed :", "25%");
                break;
            case 2:
                leftPower = gamepad1.left_stick_y;
                rightPower = gamepad1.left_stick_y;
                telemetry.addData("drive_speed :", "100%");
                break;

            case 3:
                leftPower = gamepad1.left_stick_y*0.5;
                rightPower = gamepad1.left_stick_y*0.5;
                telemetry.addData("drive_speed :", "50%");
                break;
            case 4:
                leftPower = gamepad1.left_stick_y*0.75;
                rightPower = gamepad1.left_stick_y*0.75;
                telemetry.addData("drive_speed :", "75%");
                break;

        }
        leftDrive.setPower(leftPower);
        rightDrive.setPower(rightPower);
        telemetry.addData("Motors: ", "left (%.2f), right (%.2f)", leftPower, rightPower);
    }

    // code tp control the motor for the arm
    public void arm_lift(){
        double armv, armv2;
        armv  = gamepad2.right_trigger;
        armv2  = gamepad2.left_trigger;

        if(armv > 0 ){
            arm.setPower(armv);
//            pull.setPower(pulley*0.50);
//            telemetry.addData("arm", "arm value: "+armv);
//            telemetry.addData("pull" , "pull: "+pulley);
        }else if(armv2 > 0){
            arm.setPower(-armv2);
//            pull.setPower(-pulley2*0.50);
//            telemetry.addData("arm", "arm value: "+armv);
//            telemetry.addData("pull" , "pull: "+pulley);
        }else{
            arm.setPower(0);
            telemetry.addData("Arm:", "Stop and Brake");
//            pull.setPower(0);
//            arm.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
//            pull.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
//            telemetry.addData("arm", "arm value: "+armv);
//            telemetry.addData("pull" , "pull: "+pulley);
        }

    }

    // code to control the arm
    public void claw(){
        // open the claw
        if(gamepad2.left_bumper){
            claw1.setPosition(1);
            claw2.setPosition(0);
            telemetry.addData("Claw:", "Open");
        }
        // close the claw
        if(gamepad2.right_bumper){
            claw1.setPosition(0.5);
            claw2.setPosition(0.5);
            telemetry.addData("Claw:", "Close");
        }

    }

    // Code to control the support pully system for the arm
    public void pull_system() {
        boolean tk1 = gamepad2.a;
        boolean tk2 = gamepad2.b;
        if (tk1) {
            pull.setPower(0.5);
        }else if(tk2){
            pull.setPower(-0.5);
        }else{
            pull.setPower(0);
            pull.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
            telemetry.addData("Support arm:", "Stop and brake");
        }

    }
}
